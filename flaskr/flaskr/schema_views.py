from flask.views import  MethodView
from flask import request, jsonify
from marshmallow import ValidationError


class SchemaView(MethodView):
    errors = {
        "no_obj_found": "Not found."
    }

    def get_schema_class(self, **kwargs):
        raise NotImplementedError

    def get(self, id):
        if not id:
            return self._get_all()
        return self._get_obj(id)

    def get_query_set(self):
        raise NotImplementedError

    def _get_all(self):
        users = self.get_query_set()
        schema = self.get_schema_class(many=True)
        return schema.jsonify(users)

    def _get_obj(self, id):
        schema = self.get_schema_class()
        instance = schema.get_instance({"id": id})
        if not instance:
            raise ValidationError({"error": self.errors["no_obj_found"]})
        return schema.dump(instance)

    def patch(self, id):
        schema = self.get_schema_class(partial=True, load_instance=True)
        data = {**{"id": id}, **request.get_json()}
        errors = schema.validate(data)
        if errors:
            raise ValidationError(errors)
        instance = schema.get_instance(data)
        if not instance:
            raise ValidationError({"error": self.errors["no_obj_found"]})
        instance = schema.load(data, instance=instance)
        schema.save(instance)
        return schema.dump(instance)

    def delete(self, id):
        schema = self.get_schema_class(partial=True, load_instance=True)
        data = {"id": id}
        instance = schema.get_instance(data)
        if not instance:
            raise ValidationError({"error": self.errors["no_obj_found"]})
        schema.instance = instance
        schema.delete()
        return jsonify({"message": "data deleted"}), 201

    def post(self):
        data = request.get_json()
        schema = self.get_schema_class(load_instance=True)
        errors = schema.validate(data)
        if errors:
            raise ValidationError(errors)
        instance = schema.load(data)
        schema.save(instance)
        return schema.jsonify(instance)
