from werkzeug.wrappers import Request, Response, ResponseStream


class AuthenticationMiddleWare:

    def __init__(self, app):
        self.app = app
        self.userName = 'Tony'
        self.password = 'IamIronMan'

    def __call__(self, environ, start_response):
        request = Request(environ)
        userName = request.authorization.get('username') if request.authorization else None
        password = request.authorization.get('password') if request.authorization else None

        # these are hardcoded for demonstration
        # verify the username and password from some database or env config variable
        if userName == self.userName and password == self.password:
            environ['user'] = {'name': 'Tony'}
            return self.app(environ, start_response)

        res = Response('You are not authorized to perform action.', mimetype='application/json', status=401)
        return res(environ, start_response)
