from flaskr import ma, db
from .models import User
from marshmallow import fields, ValidationError
from flaskr.schema_mixins import SaveAndDeleteSchemaMixin


def check_user_by_username(username:str, user_id=None):
    if user_id:
        return db.session.query(User).filter(User.username==username, User.id!=user_id).first()
    return db.session.query(
            User.query.filter_by(username=username).exists()
        ).scalar()


class UserSchema(ma.SQLAlchemyAutoSchema, SaveAndDeleteSchemaMixin):
    class Meta:
        model = User
        fields = ["id", "username", "first_name"]
        include_fk = True

    first_name = fields.String(required=True)

    def validate(self, data, **kwargs):
        if check_user_by_username(data.get("username"), data.get("id")):
            raise ValidationError({"username": "User already exists with username"})
        validate = super(UserSchema, self).validate(data, **kwargs)
        return validate
