import functools

from flask import Blueprint
from flask import flash
from flask import g
from flask import redirect
from flask import render_template
from flask import request, jsonify
from flask import session
from flask import url_for

from flaskr import db
from flaskr.auth.models import User
from flask.views import MethodView
from marshmallow import ValidationError
from flaskr.router import register_api
from flaskr.schema_views import SchemaView
bp = Blueprint("auth", __name__, url_prefix="/auth")


def login_required(view):
    """View decorator that redirects anonymous users to the login page."""

    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            return jsonify({"message":"Login required"}), 403

        return view(**kwargs)

    return wrapped_view


@bp.before_app_request
def load_logged_in_user():
    """If a user id is stored in the session, load the user object from
    the database into ``g.user``."""
    user_id = session.get("user_id")
    g.user = User.query.get(user_id) if user_id is not None else None


@bp.route("/register", methods=("GET", "POST"))
def register():
    """Register a new user.

    Validates that the username is not already taken. Hashes the
    password for security.
    """
    if request.method == "POST":
        username = request.form["username"]
        password = request.form["password"]
        error = None

        if not username:
            error = "Username is required."
        elif not password:
            error = "Password is required."
        elif db.session.query(
                User.query.filter_by(username=username).exists()
        ).scalar():
            error = f"User {username} is already registered."

        if error is None:
            # the name is available, create the user and go to the login page
            db.session.add(User(username=username, password=password))
            db.session.commit()
            return redirect(url_for("auth.login"))

        flash(error)

    return render_template("auth/register.html")


@bp.route("/login", methods=("GET", "POST"))
def login():
    """Log in a registered user by adding the user id to the session."""
    if request.method == "POST":
        username = request.form["username"]
        password = request.form["password"]
        error = None
        user = User.query.filter_by(username=username).first()

        if user is None:
            error = "Incorrect username."
        elif not user.check_password(password):
            error = "Incorrect password."

        if error is None:
            # store the user id in a new session and return to the index
            session.clear()
            session["user_id"] = user.id
            return redirect(url_for("index"))

        flash(error)

    return render_template("auth/login.html")


@bp.route("/logout")
def logout():
    """Clear the current session, including the stored user id."""
    session.clear()
    return redirect(url_for("index"))


# class SchemaView(MethodView):
#     errors = {
#         "no_obj_found": "User not found with ID"
#     }
#
#     def get_schema_class(self, **kwargs):
#         from .schemas import UserSchema
#         return UserSchema(**kwargs)
#
#     def get(self, id):
#         if not id:
#             return self._get_all_users()
#         return self._get_user(id)
#
#     def get_query_set(self):
#         return User.query.all()
#
#     def _get_all_users(self):
#         users = self.get_query_set()
#         user_scheme = self.get_schema_class(many=True)
#         return user_scheme.jsonify(users)
#
#     def _get_user(self, id):
#         user_scheme = self.get_schema_class()
#         instance = user_scheme.get_instance({"id": id})
#         if not instance:
#             raise ValidationError({"user": self.errors["no_obj_found"]})
#         return user_scheme.dump(instance)
#
#     def patch(self, id):
#         user_scheme = self.get_schema_class(partial=True, load_instance=True)
#         data = {**{"id": id}, **request.get_json()}
#         errors = user_scheme.validate(data)
#         if errors:
#             raise ValidationError(errors)
#         instance = user_scheme.get_instance(data)
#         if not instance:
#             raise ValidationError({"user": self.errors["no_obj_found"]})
#         instance = user_scheme.load(data, instance=instance)
#         user_scheme.save(instance)
#         return user_scheme.dump(instance)
#
#     def delete(self, id):
#         user_scheme = self.get_schema_class(partial=True, load_instance=True)
#         data = {"id": id}
#         instance = user_scheme.get_instance(data)
#         if not instance:
#             raise ValidationError({"user": self.errors["no_obj_found"]})
#         user_scheme.instance = instance
#         user_scheme.delete()
#         return jsonify({"message": "data deleted"}), 201
#
#     def post(self):
#         data = request.get_json()
#         password = data.pop("password", None)
#         user_scheme = self.get_schema_class(load_instance=True)
#         if not password:
#             raise ValidationError({"password": "Please provide password."})
#         errors = user_scheme.validate(data)
#         if errors:
#             raise ValidationError(errors)
#         instance = user_scheme.load(data)
#         instance.password = password
#         user_scheme.save(instance)
#         return user_scheme.jsonify(instance)



class UserAPI(SchemaView):


    def get_query_set(self):
        return User.query.all()

    def get_schema_class(self, **kwargs):
        from .schemas import UserSchema
        return UserSchema(**kwargs)

    def post(self):
        data = request.get_json()
        password = data.pop("password", None)
        user_scheme = self.get_schema_class(load_instance=True)
        if not password:
            raise ValidationError({"password": "Please provide password."})
        errors = user_scheme.validate(data)
        if errors:
            raise ValidationError(errors)
        instance = user_scheme.load(data)
        instance.password = password
        user_scheme.save(instance)
        return user_scheme.jsonify(instance)


register_api(bp, UserAPI, 'user_ap', '/ap/users/')
