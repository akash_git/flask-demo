class SaveAndDeleteSchemaMixin:
    """mixin for the saving object and deleting the object """

    def save(self, instance):
        self.session.add(instance)
        self.session.commit()

    def delete(self):
        self.session.delete(self.instance)
        self.session.commit()
