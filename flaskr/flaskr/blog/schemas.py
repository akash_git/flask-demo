from flaskr import ma, db
from .models import Post
from flask import request
from flaskr.auth.schemas import UserSchema
from marshmallow import fields, ValidationError
from flask_marshmallow.fields import Hyperlinks, URLFor as BaseUrlFor
from flaskr.schema_mixins import SaveAndDeleteSchemaMixin


class UrlFor(BaseUrlFor):

    def __init__(self, *args, **kwargs):
        self.domain = kwargs.pop("domain", request.host_url.rstrip(request.host_url[-1]))
        super(UrlFor, self).__init__(*args, **kwargs)

    def _serialize(self, value, key, obj):
        url = super(UrlFor, self)._serialize(value, key, obj)
        return self.domain + url


def is_valid_author_id(value):
    from flaskr.auth.models import User
    user = db.session.query(User).filter(User.id == value).first()
    if not user:
        raise ValidationError("No author found with provided author_id.")


class PostSchemas(ma.SQLAlchemyAutoSchema, SaveAndDeleteSchemaMixin):
    class Meta:
        model = Post
        fields = ('id', 'title', 'created', 'body', "author", "update_url", "_links", "delete_url", "author_id")

    author = ma.Nested(UserSchema)
    author_id = fields.Integer(required=True, validate=is_valid_author_id)
    created = fields.DateTime(format="%m/%d/%Y, %H:%M:%S")
    _links = Hyperlinks(
        {
            'author_detail': UrlFor('auth.user_ap', values=dict(id='<author_id>'),),
        }
    )
