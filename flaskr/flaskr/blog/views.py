from flask import Blueprint
from flask import flash
from flask import g
from flask import redirect
from flask import render_template
from flask import request
from flask import url_for
from werkzeug.exceptions import abort

from flaskr import db
from flaskr.auth.views import login_required
from flaskr.blog.models import Post
from flaskr.router import register_api
from flaskr.schema_views import SchemaView

bp = Blueprint("blog", __name__)


@bp.route("/")
def index():
    """Show all the posts, most recent first."""
    posts = Post.query.order_by(Post.created.desc()).all()
    import pdb
    pdb.set_trace()
    return render_template("blog/index.html", posts=posts)


def get_post(id, check_author=True):
    """Get a post and its author by id.

    Checks that the id exists and optionally that the current user is
    the author.

    :param id: id of post to get
    :param check_author: require the current user to be the author
    :return: the post with author information
    :raise 404: if a post with the given id doesn't exist
    :raise 403: if the current user isn't the author
    """
    post = Post.query.get_or_404(id, f"Post id {id} doesn't exist.")

    # if check_author and post.author != g.user:
    #     abort(403)

    return post


@bp.route("/create", methods=("GET", "POST"))
@login_required
def create():
    """Create a new post for the current user."""
    if request.method == "POST":
        title = request.form["title"]
        body = request.form["body"]
        error = None

        if not title:
            error = "Title is required."

        if error is not None:
            flash(error)
        else:
            db.session.add(Post(title=title, body=body, author=g.user))
            db.session.commit()
            return redirect(url_for("blog.index"))

    return render_template("blog/create.html")


@bp.route("/<int:id>/update", methods=("GET", "POST"))
@login_required
def update(id):
    """Update a post if the current user is the author."""
    post = get_post(id)

    if request.method == "POST":
        title = request.form["title"]
        body = request.form["body"]
        error = None

        if not title:
            error = "Title is required."

        if error is not None:
            flash(error)
        else:
            post.title = title
            post.body = body
            db.session.commit()
            return redirect(url_for("blog.index"))

    return render_template("blog/update.html", post=post)


@bp.route("/<int:id>/delete", methods=("POST",))
def delete(id):
    """Delete a post.

    Ensures that the post exists and that the logged in user is the
    author of the post.
    """
    post = get_post(id)
    db.session.delete(post)
    db.session.commit()
    return redirect(url_for("blog.list_posts", user_id=post.author_id))


# @bp.route("/api/posts/<int:user_id>")
# def list_posts(user_id):
#     from .schemas import PostSchemas
#     posts = Post.query.filter_by(author_id=user_id).all()
#     post_schema = PostSchemas(many=True)
#     return post_schema.jsonify(posts), 200
#
#
# @bp.route("/api/posts", methods = ["POST"])
# def create_posts():
#     from .schemas import PostSchemas
#     from marshmallow import ValidationError
#     data = request.get_json()
#     post_schema = PostSchemas(load_instance=True)
#     errors = post_schema.validate(data)
#     if errors:
#         raise ValidationError(errors)
#     instance = post_schema.load(data)
#     post_schema.session.add(instance)
#     post_schema.session.commit()
#     return post_schema.jsonify(instance), 201


class PostView(SchemaView):

    def get_query_set(self):
        return Post.query.all()

    def get_schema_class(self, **kwargs):
        from .schemas import PostSchemas
        return PostSchemas(**kwargs)

register_api(bp, PostView, 'post_ap', '/ap/posts/')
